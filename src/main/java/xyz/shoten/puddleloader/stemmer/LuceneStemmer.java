package xyz.shoten.puddleloader.stemmer;




import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sivan on 7/10/16.
 */

public class LuceneStemmer implements Serializable{

   // static String stop_word_set="english.stop.txt";
    static List<String> stop_w=new ArrayList<>();

    public static void loadStopWord(String stop_word_set) throws IOException {
        BufferedReader r=new BufferedReader(new FileReader(stop_word_set));
        String word="";

        while((word=r.readLine())!=null){
            stop_w.add(word);
        }
    }


    public static  String tokenizeStopStem(String input) throws IOException {



        StandardTokenizer standardTokenizer = new StandardTokenizer();
        standardTokenizer.setReader(new StringReader(input));
        standardTokenizer.reset();

        TokenStream tokenStream = new StopFilter( standardTokenizer, StopFilter.makeStopSet(stop_w,true));
        tokenStream = new PorterStemFilter(tokenStream);

        StringBuilder sb = new StringBuilder();
        // OffsetAttribute offsetAttribute = tokenStream.addAttribute(OffsetAttribute.class);
        CharTermAttribute charTermAttr = tokenStream.getAttribute(CharTermAttribute.class);
        try{
            while (tokenStream.incrementToken()) {
                if (sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(charTermAttr.toString());
            }
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        return sb.toString();
    }






}
