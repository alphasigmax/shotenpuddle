package xyz.shoten.puddleloader;

/* SimpleApp.java */

import org.apache.hadoop.io.Text;
import org.apache.nutch.parse.ParseText;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;

public class SimpleApp {
    long serialVersionUID = 6732270565076291202l;
    public static void main(String[] args) throws ClassNotFoundException {
        SparkConf conf = new SparkConf().setAppName("Simple Application").setMaster("spark://localhost:7077");
        //conf.set("spark.kryoserializer.buffer.mb","10");
        //conf.set("spark.cleaner.ttl","43200");
       conf.registerKryoClasses(new Class<?>[]{
            Class.forName("org.apache.nutch.parse.ParseText"),
            Class.forName("org.apache.hadoop.io.Text")
        });
        JavaSparkContext sc = new JavaSparkContext(conf);
        sc.addJar("/Users/sivan/develop/shotenpuddle/build/libs/puddle-loader-0.0.1-SNAPSHOT.jar");
        JavaPairRDD<Text,ParseText> logData = sc.sequenceFile("hdfs://localhost:8020/mnt/nutch/keyvalue",Text.class, ParseText.class);
       // JavaRDD<String> textFile = sc.textFile("hdfs://localhost:8020/mnt/input/rawdata/README.txt");
        logData
            .reduceByKey(new Function2<ParseText, ParseText, ParseText>() {

                @Override
                public ParseText call(ParseText text, ParseText parseText) throws Exception {
                    System.out.println("Lines with a: " + text.getText());
                    return text;
                }
            });

       /* JavaRDD<String> words = textFile
            .flatMap(new FlatMapFunction<String, String>() {
                public Iterable<String> call(String s) {
                    return Arrays.asList(s.split(" "));
                }
            });

        JavaPairRDD<String, Integer> pairs = words
            .mapToPair(new PairFunction<String, String, Integer>() {
                public Tuple2<String, Integer> call(String s) {
                    return new Tuple2<String, Integer>(s, 1);
                }
            });
        JavaPairRDD<String, Integer> counts = pairs
            .reduceByKey(new Function2<Integer, Integer, Integer>() {
                public Integer call(Integer a, Integer b) {
                    return a + b;
                }
            });
*/
         //  System.out.println("Lines with a: " +l);
        logData.saveAsTextFile("hdfs://localhost:8020/mnt/output/test1");

    }
}
