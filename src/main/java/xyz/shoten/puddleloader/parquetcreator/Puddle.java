package xyz.shoten.puddleloader.parquetcreator;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Created by spyder on 28/07/16.
 */

public  class Puddle implements Serializable {

    Puddle() {
        lastCrawledTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    }

    private String siteName;
    private String text;
    private Long lastCrawledTime;

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getLastCrawledTime() {
        return lastCrawledTime;
    }

    public void setLastCrawledTime(Long lastCrawledTime) {
        this.lastCrawledTime = lastCrawledTime;
    }

}
