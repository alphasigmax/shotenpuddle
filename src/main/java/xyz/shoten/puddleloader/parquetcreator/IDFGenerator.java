package xyz.shoten.puddleloader.parquetcreator;

import org.apache.spark.SparkConf;

import org.apache.spark.ml.feature.*;

import org.apache.spark.ml.linalg.Vector;
//import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.List;


/**
 * Created by spyder on 3/08/16.
 */

public class IDFGenerator {

    public static String SPARK_MASTER = "local";

    public static void main(String[] args) {

        SparkConf conf = new SparkConf().setAppName("DataLakeMaker").setMaster(SPARK_MASTER);
        SparkSession spark = SparkSession
            .builder()
            .config(conf)
            //   .config(ParquetOutputFormat.WRITER_VERSION, ParquetProperties.WriterVersion.PARQUET_2_0.toString())
            .getOrCreate();

        System.out.println("Spark Initialization complete");
        Dataset<Row> sentenceData = spark.read().parquet("hdfs://localhost:9000/user/spyder/puddle/datalake.parquet");
        sentenceData.explain();

        Tokenizer tokenizer = new Tokenizer().setInputCol("text").setOutputCol("words");
        Dataset<Row> wordsData = tokenizer.transform(sentenceData);

        HashingTF hashingTF = new HashingTF()
            .setInputCol("words")
            .setOutputCol("rawFeatures");
        //.setNumFeatures(numFeatures);
        Dataset<Row> featurizedData = hashingTF.transform(wordsData);
        //featurizedData.show(false);
        featurizedData.write().mode(SaveMode.Overwrite).parquet("hdfs://localhost:9000/user/spyder/puddle/termfrequency.parquet");

//        CountVectorizerModel cvModel = new CountVectorizer()
//            .setInputCol("words")
//            .setOutputCol("rawFeaturesCV")
//            .setVocabSize(3)
//            .setMinDF(2)
//            .fit(df);

        IDF idf = new IDF().setInputCol("rawFeatures").setOutputCol("features");
        IDFModel idfModel = idf.fit(featurizedData);
        Dataset<Row> rescaledData = idfModel.transform(featurizedData);

        rescaledData.write().mode(SaveMode.Overwrite).parquet("hdfs://localhost:9000/user/spyder/puddle/indexedata.parquet");

        for (Row r : rescaledData.select("features", "siteName", "rawFeatures", "words").takeAsList(3)) {
            Vector features = r.getAs(0);
            String label = r.getString(1);
            Vector rawfeatures = r.getAs(2);
            List words = r.getList(3);
            System.out.println("sitename=" + label);
            System.out.println("WOR Features= " + (words));
            System.out.println("RAW Features= " + rawfeatures);
            System.out.println("NOR Features= " + features);


        }
    }
}
