package xyz.shoten.puddleloader.parquetcreator;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import xyz.shoten.puddleloader.stemmer.LuceneStemmer;
import org.apache.spark.ml.feature.RegexTokenizer;
import org.apache.spark.ml.feature.Tokenizer;

import java.io.IOException;
import java.util.List;
import static org.apache.commons.lang3.StringEscapeUtils.unescapeJava;

/**
 * Created by spyder on 28/07/16.
 * This class moves all the dirty data from puddle to datalake clean
 */
public class RawToClean {



   static final StanfordCoreNLP pipeline = new StanfordCoreNLP(
        PropertiesUtils.asProperties(
            "annotators", "tokenize,ssplit,pos,lemma,ner,parse,natlog",
            "ssplit.isOneSentence", "false",
            "parse.model", "edu/stanford/nlp/models/srparser/englishSR.ser.gz",
            "ner.model","edu/stanford/nlp/models/ner/english.all.3class.caseless.distsim.crf.ser.gz",
            "tokenize.language", "en"));

    public static String SPARK_MASTER = "spark://localhost:7077";

    RawToClean()
    {
       // props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");

    }

    private static String cleanString(String rawString)
    {
        rawString = unescapeJava(rawString);
        return rawString ;//.replaceAll("[^a-zA-Z0-9]", " ");
    }

    public static void main(String[] args) throws ClassNotFoundException, IOException {

        SPARK_MASTER = "local";
        SparkConf conf = new SparkConf().setAppName("DataLakeMaker").setMaster(SPARK_MASTER);
        SparkSession spark = SparkSession
            .builder()
            .config(conf)
            //   .config(ParquetOutputFormat.WRITER_VERSION, ParquetProperties.WriterVersion.PARQUET_2_0.toString())
            .getOrCreate();

        JavaSparkContext sparkContext = new JavaSparkContext(spark.sparkContext());


        System.out.println("Spark Initialization complete");
        Dataset<Row> reader = spark.read().parquet("hdfs://localhost:9000/user/spyder/puddle/puddle.parquet");
        long count = reader.count();

        Encoder<DataLake> datalakeEncoder = Encoders.bean(DataLake.class);
        System.out.println(" Number of total records =" + count);


        //TODO move this to hadoop common area!
        LuceneStemmer.loadStopWord("/media/spyder/BitEast/project/shoten/stopwords_eng.txt");

        Dataset<DataLake> dataLakeDataSet = reader.map(new MapFunction<Row, DataLake>() {

            @Override
            public DataLake call(Row value) throws Exception {
             //   System.out.println(" reading row=" + value);

                DataLake dataLake = new DataLake();
                dataLake.setLastCrawledTime(value.getLong(0));
                dataLake.setSiteName(value.getString(1));
                String rawstring = value.getString(2).trim();
                doStanford(dataLake, cleanString(rawstring));
               // System.out.println(" raw string =" + rawstring);
              //  System.out.println(" Cleaned NLP string =" + dataLake.getText());

                return dataLake;
            }
        }, datalakeEncoder);

        dataLakeDataSet.collect();
        dataLakeDataSet.show(false);

        dataLakeDataSet.write().mode(SaveMode.Overwrite).parquet("hdfs://localhost:9000/user/spyder/puddle/datalake.parquet");
    }

    private static void doStanford(  DataLake dataLake,String stringtoparse)
    {
        String stemmed = "";


        // create an empty Annotation just with the given text
        Annotation document = new Annotation(stringtoparse);

// run all Annotators on this text
        pipeline.annotate(document);

        // these are all the sentences in this document
// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

        for(CoreMap sentence: sentences) {


            System.out.println(" NLP found sentence= row=" + sentence.toString() );

            // traversing the words in the current sentence
            // a CoreLabel is a CoreMap with additional token-specific methods
            for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                // this is the text of the token
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                // this is the POS tag of the token
                String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                // this is the NER label of the token
                String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
                String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
                String tokenizedword = word;
                try {
                    //TODO only tokenize VERBS etc? Not NNP
                    if(!pos.equalsIgnoreCase("NNP"))
                    tokenizedword = LuceneStemmer.tokenizeStopStem(word);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(" NLP Parsed sendtence word/tokenizedword(lemms)/pos/ne  ::==>" + word + "/" + tokenizedword + "("  + lemma + ")/" + pos + "/"+ ne);
               if(tokenizedword.trim().length()>0)
                stemmed = stemmed + " " + tokenizedword.trim();
            }

            dataLake.setText(stemmed);
            // this is the parse tree of the current sentence
            Tree tree = sentence.get(TreeCoreAnnotations.TreeAnnotation.class);

            // this is the Stanford dependency graph of the current sentence
            SemanticGraph dependencies = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
        }
    }
}

