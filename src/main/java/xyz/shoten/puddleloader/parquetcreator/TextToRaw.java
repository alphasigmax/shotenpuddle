package xyz.shoten.puddleloader.parquetcreator;

import org.apache.parquet.column.ParquetProperties;
import org.apache.parquet.hadoop.ParquetOutputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.io.IOException;

/**
 * Created by spyder on 28/07/16.
 */
public class TextToRaw {


    public static String SPARK_MASTER = "spark://localhost:7077";

    public static long serialVersionUID = 6732270565076291202L;

    public static void main(String[] args) throws ClassNotFoundException, IOException {
        SPARK_MASTER = "local";
        SparkConf conf = new SparkConf().setAppName("Simple Application").setMaster(SPARK_MASTER);
        SparkSession spark = SparkSession
            .builder()
            .config(conf)
        //    .config(ParquetOutputFormat.WRITER_VERSION, ParquetProperties.WriterVersion.PARQUET_2_0.toString())
            .getOrCreate();

        JavaSparkContext sparkContext = new JavaSparkContext(spark.sparkContext());


        System.out.println("Spark Initialization complete");
        JavaPairRDD<String, String> javapairrdd = sparkContext.wholeTextFiles("hdfs://localhost:9000/user/spyder/sampleinput/*");

        JavaRDD<Puddle> puddlerdd = javapairrdd.map((Function<Tuple2<String, String>, Puddle>) tuple ->
            {
                System.out.println("Pair RDD pairing " + tuple._1());
                Puddle puddle = new Puddle();
                puddle.setSiteName(tuple._1());
                puddle.setText(tuple._2());
                return puddle;
            }
        );

        Dataset<Row> puddleDF = spark.createDataFrame(puddlerdd, Puddle.class);
        puddleDF.write().mode(SaveMode.Overwrite).parquet("hdfs://localhost:9000/user/spyder/puddle/puddle.parquet");
        System.out.println(" Program complete");
spark.stop();


    }
}


