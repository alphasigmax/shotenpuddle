package xyz.shoten.puddleloader.parquetcreator;

import java.io.Serializable;


/**
 * Created by spyder on 28/07/16.
 */
public class DataLake implements Serializable {


    private String siteName; //unique id
    private String text;
    private Long lastCrawledTime;
/*
rank (depending on how many links to it?)

 */
    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getLastCrawledTime() {
        return lastCrawledTime;
    }

    public void setLastCrawledTime(Long lastCrawledTime) {
        this.lastCrawledTime = lastCrawledTime;
    }

    @Override
    public String toString() {
        return "DataLake{" +
            "siteName='" + siteName + '\'' +
            ", text='" + text.substring(0,100) + "..." + '\'' +
            ", lastCrawledTime=" + lastCrawledTime +
            '}';
    }
}
