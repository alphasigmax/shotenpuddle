package xyz.shoten.puddleloader.mapreduce;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.StringTokenizer;

/**
 * Mapreduce to generate bagofwords using lucene
 * Created by sivan on 7/11/16.
 */
public class BagofWordMapReduce  {

    public static class BWMap extends Mapper<LongWritable,Text,Text, LongWritable>{

       // private LuceneParser luceneParser=new LuceneParser();
        private Text word = new Text();
        private LongWritable lw=new LongWritable(1);

     /*   public void setup(Context context){
            try {
                luceneParser.loadStopWord();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }*/


        public void map(LongWritable key,Text value,Context context) throws IOException, InterruptedException {
            String stemKey="";//luceneParser.tokenizeStopStem(value.toString());
            StringTokenizer st=new StringTokenizer(stemKey);
            while(st.hasMoreElements()){
                word.set(st.nextToken());
                context.write(word,lw);
            }

        }

    }

    public static class BWReduce extends Reducer<Text,LongWritable,Text,LongWritable>{

        LongWritable sumwriteable=new LongWritable();

        @Override
        protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            for(LongWritable value:values){
                sum+=value.get();
            }
            sumwriteable.set(sum);
            context.write(key,sumwriteable);

        }


    }

    public static void main(String ...a) throws IOException, ClassNotFoundException, InterruptedException, URISyntaxException {

        if(a.length<2){
            System.out.println("Please enter input and output path");
            System.exit(0);
        }
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Bag Of Word");
        job.setJarByClass(BagofWordMapReduce.class);
        job.setMapperClass(BWMap.class);
        job.setReducerClass(BWReduce.class);
        //job.setInputFormatClass(TextInputFormat.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        FileInputFormat.addInputPath(job,new Path(a[0]));
        FileOutputFormat.setOutputPath(job,new Path(a[1]));
        job.addFileToClassPath(new Path("/mnt/shoten/lib/lucene-core-6.1.0.jar"));
        job.addFileToClassPath(new Path("/mnt/shoten/lib/lucene-analyzers-common-6.1.0.jar"));
        job.addCacheFile(new URI("/mnt/lucene/english.stop.txt#english.stop.txt"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }



}
