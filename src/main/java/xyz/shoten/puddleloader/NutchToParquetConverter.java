package xyz.shoten.puddleloader;

import org.apache.hadoop.io.Text;
import org.apache.nutch.parse.ParseText;
import org.apache.parquet.column.ParquetProperties;
import org.apache.parquet.hadoop.ParquetOutputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkFiles;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;
import xyz.shoten.puddleloader.stemmer.LuceneStemmer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sivan on 7/15/16.
 */
public class NutchToParquetConverter {

    public static String HDFS_CLUSTER="hdfs://localhost:8020";
    public static String SPARK_MASTER="spark://localhost:7077";

    long serialVersionUID = 6732270565076291202l;

    public static void main(String[] args) throws ClassNotFoundException, IOException {
       SparkConf conf = new SparkConf().setAppName("Simple Application").setMaster(SPARK_MASTER);
        conf.registerKryoClasses(new Class<?>[]{
            Class.forName("org.apache.nutch.parse.ParseText"),
            Class.forName("org.apache.hadoop.io.Text"),
            Class.forName("xyz.shoten.puddleloader.stemmer.LuceneStemmer")
        });
        JavaSparkContext sc = new JavaSparkContext(conf);

        sc.addJar("/Users/sivan/develop/shotenpuddle/build/libs/puddle-loader-0.0.1-SNAPSHOT.jar");
        sc.addFile("/mnt/lucene/english.stop.txt");
        JavaPairRDD<Text,ParseText> logData = sc.sequenceFile(HDFS_CLUSTER+"/mnt/nutch/keyvalue",Text.class, ParseText.class);
//spark.executor.extraClassPath

        // The schema is encoded in a string
        String schemaString = "url content";
        String s=SparkFiles.get("english.stop.txt");
        LuceneStemmer.loadStopWord(s);
        System.out.println("file :"+s);
        // Generate the schema based on the string of schema
        List<StructField> fields = new ArrayList<StructField>();
        for (String fieldName: schemaString.split(" ")) {
            fields.add(DataTypes.createStructField(fieldName, DataTypes.StringType, true));
        }

        // Convert records of the RDD (people) to Rows.
        JavaRDD<Row> rowRDD = logData.map(
            new Function<Tuple2<Text, ParseText>, Row>() {
                public Row call(Tuple2<Text, ParseText> record) throws Exception {
                       String stemedword=LuceneStemmer.tokenizeStopStem(record._2.getText().trim());
                    return RowFactory.create(record._1.toString(),stemedword);
                }
            });

        SparkSession spark = SparkSession
            .builder()
            .config(conf)
        .config(ParquetOutputFormat.WRITER_VERSION , ParquetProperties.WriterVersion.PARQUET_2_0.toString())
        .getOrCreate();

        StructType schema = DataTypes.createStructType(fields);


        // Apply the schema to the RDD.
        Dataset<Row> peopleDataFrame = spark.createDataFrame(rowRDD, schema);

        long l=System.currentTimeMillis();

        String path=HDFS_CLUSTER+"/mnt/output/test1/employee"+l+".parquet";
        peopleDataFrame.write().parquet(path);


       //read from parquet file
      Dataset nd= spark.read().parquet(path);
        // Register the DataFrame as a table.
        nd.registerTempTable("nutchCrawl");
        // SQL can be run over RDDs that have been registered as tables.
        Dataset results = spark.sql("SELECT content FROM nutchCrawl");

        List<String> names = results.javaRDD().map(new Function<Row, String>() {
            public String call(Row row) {
                return "URL: " + row.getString(0);
            }
        }).collect();

        System.out.println("URL's :"+names);
    }

}
